package com.nimbusds.srp6;


import java.math.BigInteger;

public class SRP6ServerStateContext {

    public final long lastActivity;

    public final String userID;

    public final BigInteger B;

    public final BigInteger u;

    public final BigInteger k;

    public final BigInteger S;

    public final BigInteger M2;

    public final BigInteger s;

    public final BigInteger v;

    public final BigInteger b;

    public final SRP6ServerSession.State state;

    public SRP6ServerStateContext(long lastActivity, String userID, BigInteger B, BigInteger u, BigInteger k, BigInteger S, BigInteger M2, BigInteger s, BigInteger v, BigInteger b, SRP6ServerSession.State state) {
        this.lastActivity = lastActivity;
        this.userID = userID;
        this.B = B;
        this.u = u;
        this.k = k;
        this.S = S;
        this.M2 = M2;
        this.s = s;
        this.v = v;
        this.b = b;
        this.state = state;
    }
}
